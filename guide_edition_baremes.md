# Quelques règles communes des fichiers « Barèmes IPP »
*(Version 15/06/2016)*

Il est souhaitable que les conventions qui suivent soient respectées avec une grande rigueur.
Cela permet d’assurer une harmonisation des fichiers quand ils sont diffusés publiquement.
Cela garantit aussi un usage efficace des outils d’extraction des paramètres législatifs (parsing).

### 1.	Organisation des fichiers :
Les variables se lisent en colonne et les années en ligne.
**Chaque nouvelle ligne correspond à un changement législatif**
(modification/actualisation, nouveauté ou suppression). Afin de pouvoir lire les fichiers par python
ou TAXIPP, les éléments suivants sont importants :
-	**La ligne 1 de chaque colonne, masquée, est un nom de variable** (unique, sans espace ni majuscule)
-	La colonne 1 comporte une date si la ligne correspondante contient des informations ;
 il n’y a rien sinon (permet d’enlever les notes en texte en-dessous des paramètres) :
**il n’y a pas de date à gauche s’il n’y a pas de données**. Attention, ne pas mettre de lignes
 et colonnes masquées (autre que la ligne 1) : sinon, l’importation des barèmes et la lecture ne sont pas bonnes.

### 2.	Format des cellules :
#### A)	Onglets des sommaires
Le texte contenu dans la case bleu clair contenant la citation suggérée,
 les noms des auteurs et les contacts doit être inséré dans une seule colonne,
  comprise entre la colonne 8 (ou « H ») et la colonne 12 (« L »).
#### B)	Onglets des barèmes
Les seuls formats tolérés sont :
-	Date excel jj/mm/aaaa
-	Pourcentage (% apparent)
-	Monétaire (€ ou FRF avant 2001) : **doit s’appliquer pour les montants monétaires**
-	Nombre
-	Chaîne de caractères

**Aucune cellule de paramètres ne doit être fusionnée** (il faut copier-coller si c’est la même valeur).
L’usage des couleurs pour les paramètres est le suivant :
-	Bleu clair signifie que l’information en question n’a plus lieu d’être (on sait que le paramètre n’est plus - ou pas encore- en vigueur). La colonne de dates est en bleu pour des raisons esthétiques.
-	**Les cellules vides signifiant « on ne sait pas » sont indiquées par « nc » (non connu)**,
 c’est-à-dire que le paramètre peut ne plus s’appliquer comme ne pas être renseigné par manque de références/sources.

**De la fusion dans les labels paramètres**
Il est possible de :
- fusionner des cellules verticalement, sachant que python lira le contenu comme provenant de la cellule du haut
- fusionner des cellules horizontalement mais **que pour la première ligne** de label (i.e. ligne 2 de l'onglet). Si des cellules ultérieures (ligne>2) sont fusionnées horizontalement, alors la colonne sera mal lue par le parser et le paramètre omis des sorties clean (ex: l'ancienne version de l'onglet FILLON).
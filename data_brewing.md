Marche à suivre pour vérifier le bon fonctionnement du data brewer et identifier les sources d'erreurs
===============================================================================

1. Dans "Projects", aller sur "French Tax and Benefits Tables / ipp-tax-and-benefit-tables-yaml-clean".

2. Cliquer sur "Merge Requets" et choisir la dernière mise à jour, ou toute autre que vous voulez inspecter.

3. Aller sur "Changes" et identifier les "Errors" et "Warnings".

4. Faire les modiciations nécessaires sur le document, le soumettre, et consulter de nouveau les potentielles erreurs.

5. Si aucune "Error" ni aucun "Warning" n'ont été trouvé, le fichier est opérationnel.
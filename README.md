# ipp-tax-and-benefit-tables-xlsx

Original IPP's tax and benefit tables in XLSX format.

IPP is the [Institut des politiques publiques](http://www.ipp.eu/en/)

Published tax and benefit tables:
- English: http://www.ipp.eu/en/tools/ipp-tax-and-benefit-tables/
- French: http://www.ipp.eu/fr/outils/baremes-ipp/

## Data License

Licence ouverte / Open Licence <http://www.etalab.gouv.fr/licence-ouverte-open-licence>

## Contribute to IPP's tax and benefit tables

If you wish to contribute, you may find the following documentation helpful (in french)

* [Contributions are welcomed](CONTRIBUTING.md)
* [Guide to read the legislation](guide_legislation.md)
* [Edition rules of tax and benefit tables](guide_edition_baremes.md)
* [Guide for commiting tables updates](guide_partage_baremes.md)

 
Les contributeurs sont invités à consulter la documentation détaillés dans les sections suivantes:

* [Les contributeurs sont les bienvenus](CONTRIBUTING.md)
* [Un petit guide pour se répérer dans la législation](guide_legislation.md)
* [Les règles d'édition des barèmes](guide_edition_baremes.md)
* [Un petit guide pour partager les barèmes mis à jour](guide_partage_baremes.md)



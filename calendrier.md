# Calendrier indicatif des publications et décisions de modifications des paramètres de la législations socio-fiscale

Le calendrier est désormais [disponible ici](https://git.framasoft.org/french-tax-and-benefit-tables/baremes-ipp-yaml/blob/master/doc/calendrier.md)
# Procédure à suivre pour l'actualisation du barème tarification-energie-logement
*(Version 26/07/2017)*

Ce barème répertorie les prix HT et TTC des principales énergies utilisées par le ménage pour leur logement.

### 1.	Tarifs HT :

#### A. Tarifs de l'électricité

- Les tarifs de l'électricité rapportés correspondent au tarif bleu, option base résidentiel. Ils sont composés d'un prix fixe pour l'abonnement, et d'un prix unitaire.

- Les tarifs sont fixés par la Commission de Régulation de l'Energie (CRE) et toutes les informations nécessaires pour compléter ces onglets sont disponibles accessibles sur [leur site] (http://www.cre.fr/marches/marche-de-detail/marche-de-l-electricite#section2).

- A noter que d'après l'article 2 de l'arrêté du 12/08/2010 (JORF n°0186 du 13 août 2010), pour les nouvelles souscriptions de compteurs de puissance supérieure ou égale à 18 kva, les tarifs base ne sont plus proposés. Les personnes disposant déjà de ces équipements peuvent toutefois conserver ces contrats.
 

#### B. Tarifs du gaz naturel

- Les tarifs du gaz rapportés correspondent aux tarifs réglementés. Quatre contrats sont proposés selon le niveau de consommation : Base, B0, B1 et B2I. Ils sont composés d'un prix fixe pour l'abonnement, et d'un prix unitaire.

- A noter également que le prix unitaire dépend de la localisation géographique, afin de mieux répercuter les différences de coût d'acheminement. Il existe 6 zones définies d'après leur distance par rapport au centre de stockage le plus proche.

- Les tarifs sont rapportés en ligne dans des documents d'Engie, mais aucune source juridique n'a été identifiée. Les dernières actualisations ont été effectuées à partir de [ce document] (https://entreprises-collectivites.engie.fr/wp-content/uploads/2016/09/ENGIE_Entreprises_Collectivites_tarifs_Distribution_Publique_septembre2016.pdf) 

#### C. Tarifs du fioul domestique

- Les tarifs du fioul domestique sont extraites d'[une base] (http://www.prix-carburants.developpement-durable.gouv.fr/petrole/se_cons_fr.htm) élaborée par la Direction Générale de l'Energie et du Climat (DGEC) du ministère de l'écologie. 


### 2.	Tarifs TTC

#### A. Tarifs de l'électricité

- Les tarifs TTC de l'électricité sont répertoriés dans la [base de données Pégase] (http://www.statistiques.developpement-durable.gouv.fr/donnees-ligne/r/pegase.html) du Commissariat Générale au Développement Durable (CGDD) et du Service de l’observation et des statistiques (SOeS).

- Les données rapportées dans ce barème correspondent aux prix annuels pour un ménage en tarif bleu option base.


#### B. Tarifs du gaz naturel

- Les tarifs TTC du gaz naturel sont également répertoriés dans la [base de données Pégase] (http://www.statistiques.developpement-durable.gouv.fr/donnees-ligne/r/pegase.html) du Commissariat Générale au Développement Durable (CGDD) et du Service de l’observation et des statistiques (SOeS).

- Les données rapportées dans ce barème correspondent aux prix annuels pour un ménage.


#### C. Tarifs du fioul domestique

- Pour les tarifs TTC du fioul domestique, ils figurent dans la [base] (http://www.prix-carburants.developpement-durable.gouv.fr/petrole/se_cons_fr.htm) qui recense les tarifs HT.

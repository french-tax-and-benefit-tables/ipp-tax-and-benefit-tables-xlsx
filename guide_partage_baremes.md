Marche à suivre pour modifier les barèmes pour les contributeurs issus de l'IPP
===============================================================================

Si ce n'est déjà fait (à ne faire que la première fois), installer une console [shell](https://git.framasoft.org/ipp/ipp-survival-gitbook/blob/master/shell.md) et créer [sa clé SSH pour s'authentifier sur le GitLab de framagit](https://framagit.org/help/ssh/README.md).

1. Rassembler les informations concernant les barèmes à modifier. Un calendrier indicatif est [disponible](https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-xlsx/blob/master/calendrier.md).

2. Ouvrir une console [shell](https://git.framasoft.org/ipp/ipp-survival-gitbook/blob/master/shell.md) 
    et se placer dans le répertoire contenant les barèmes (ne pas hésiter à 
    utiliser la touche <kbd>Tab</kbd> pour faire apparaître les noms des sous-dossiers.)

    ````shell
    cd "Z:\3-Legislation\Baremes IPP"
    ````
   L'invite de la console nous permet de vérifier que l'on effectue bien des modifications
   sur la branche `master` en local (i.e. sur notre ordinateur).
   
    ````bash
     /z/3-Legislation/Baremes IPP (master)
    ````
3. Récupérer les informations sur l'ensemble des dépôts distants, i.e. récupérer les modifications validées effectuées par les autres utilisateurs.

    ````shell
    git fetch --all
    ````
4. Vérifier si la branche locale (i.e. sur notre ordinateur) est en retard par rapport à la branche du dépôt IPP sur Gitlab (appelée ipp/master) en éxecutant:

    ````shell
    gitk --all
    ````

5. Si la branche locale est en retard, afin de se synchroniser avec la branche du [dépôt de l'IPP situé sur le serveur gitlab hébergé par framasoft](https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-xlsx/), on importe les modifications propagées par les autres utilisateurs en éxecutant:    
    
    ````shell
    git rebase origin/master
    ````    
6. Vérifier l'état du dépôt en exécutant: 
   
    ````shell
    git status 
    ````
    Cela nous permet de voir si des fichiers suivis ("tracked files") ont été modifiés sans avoir été commités.
7. Si les fichiers à modifier sont inchangés allez au point directement au point 8.
   Si un fichier à modifier est altéré depuis le dernier "commit", commiter les modifications en éxecutant:
    
    ````shell
    git commit
    ````

8. Modifier son fichier excel (après avoir consulter [ces instructions](guide_edition_baremes.md) ), enregistrer les modifications, puis fermer le document.

9. Puis en utilisant le shell, enregistrer ses modifications dans le système de version local
   en prenant soin d'effectuer les deux étapes suivantes:
  * n'ajouter que le/les fichier(s) modifiés à l'index en utilisant: 

    ````shell
    git add monfichier.xlsx
    ````
    ou en utilisant l'interface accessible en exécutant `git-gui`

    ````shell
    git gui
    ````
  * puis commiter les modifications en ajoutant un message de "commit" [compréhensible par tous](http://chris.beams.io/posts/git-commit/) 
    indiquant les modifications effectuées en exécutant
    

    ````shell
    git commit
    ````

    S'il y a lieu, ne pas oublier 
    d'indiquer le ticket ("issue") qui est ainsi fermé en ajoutant dans le message
    de commit le texte ci-dessous:
    
    ````txt
    Fais plein de modifications très utiles
    Closes #NumeroDuTicket`
    ````

10. Enfin, propager les modifications au [dépot gitlab hébergé par framasoft](https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-xlsx/)
   en exécutant
    
   ````shell
   git push ipp master
   ````

   ou en utilisant l'interface accessible en exécutant `git-gui`

   ````shell
   git gui
   ````

11. Pour bien vérifier que ma version en local est parfaitement synchronisée avec la version à jour sur Gitlab, faire une vérification en éxécutant à nouveau:
    
````shell
gitk --all
````

12. Normalement, la fenêtre affiche que ma version locale est bien synchronisée avec le repo `origin/master`, mais pas avec `ipp/master`. Ces deux repos pointent exactement au même endroit. L'intérêt est juste ici d'avoir deux repos correspondant chacun à un protocole distinct. Ces deux repos pointant au même endroit, la bonne synchronisation de ma version locale avec `origin/master` implique forcément celle avec `ipp/master`. C'est juste que git considère que seul le repo sur lequel le push a été fait a été affecté. Afin de palier ce problème "d'affichage", Il faut donc récupérer l'ensemble de l'information distante existante en faisant :

````shell
git fetch --all
````

Puis, on fait :

````shell
gitk --all
````

La fenêtre doit normalement afficher la bonne syncronisation de ma version locale avec les deux repos distants existants.

13. Un des objectifs de ce projet est de publier l'ensemble des barèmes IPP actualisés chaque année. Il est donc important de mettre une "étiquette" à l'endroit correspondant à la version du repo qui a été envoyée pour publication. Une telle étiquette s'appelle un "tag". A partir du shell, il est possible d'obtenir une liste de l'ensemble des tags qui ont déjà été apposés en tapant :

    ````shell
    git tag
    ````

14. Une fois achevée l'ensemble de l'actualisation des barèmes, et au moment d'envoyer les barèmes pour publication, il faut une personne chargée de créer un tag. Il suffit de taper :

    ````shell
    git tag yyyy_mm_release
    ````

où `yyyy_mm_release` correspond au nom que l'on veut donner au tag (ici par exemple, on veut étiqueter la version qui a été publiée le mois `mm` de l'année `yyyy`).

15. A ce stade, le tag n'est mis qu'au niveau local. Pour le pusher sur le repo distant, il faut taper :

    ````shell
    git push --tags
    ````
    
16. Il est possible de vouloir supprimer un tag existant, au cas où l'on veuille changer au dernier moment la version à publier (en supprimant puis en recréant le tag). Pour supprimer le tag en local, taper :

    ````shell
    git tag -d yyyy_mm_release
    ````

17. Enfin, pour supprimer le tag sur le repo distant, taper :

    ````shell
    git push origin master :refs/tags/yyyy_mm_release
    ````



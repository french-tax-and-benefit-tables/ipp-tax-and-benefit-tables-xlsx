## Guide de la législation socio-fiscale française 
#### (pour ajout de références législatives dans les barèmes IPP)



Les barèmes IPP doivent renseigner, pour chaque paramètre concerné, chaque modification de sa valeur, avec la référence dans les textes de loi associée à cette valeur.

Un dispositif donné est régi par une partie législative (la loi), et une partie réglementaire (décrets, arrêtés, etc.). Les règles générales d’un dispositif (ex : de quels facteurs dépend le dispositif) sont inscrites dans la loi. Puis, les règles opérationnelles (quelle application de la loi ?) sont la plupart du temps décidées par décret ou arrêté. Ainsi, la plupart du temps, les formules de calcul des prélèvements ou transferts sont renseignées dans la loi, et les paramètres par décret ou arrêté. 

Le site [www.legifrance.gouv.fr](https://www.legifrance.gouv.fr/) contient l’ensemble des textes de loi, législatifs ou règlementaire, avec tous leurs historiques depuis plusieurs dizaines d’années. C’est donc le site qui permet de connaître l’ensemble des références à inscrire sur les barèmes IPP.

Concernant la partie législative, un dispositif a ses règles générales inscrites dans la loi. Par exemple, les allocations familiales sont régies par différents articles de loi, dont le premier est l’article L521-1 du code de la sécurité sociale (CSS), qui se trouve [ici](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=19EA4D92D6B2F82DA7DF807F71CF39C1.tplgfr21s_3?idArticle=LEGIARTI000029963006&cidTexte=LEGITEXT000006073189&categorieLien=id&dateTexte=).

Un article de loi peut changer au cours du temps, et être modifié par d’autres articles : par exemple, sur le lien ci-dessus de l’article L521-1 du CSS, on voit à gauche de la page que l’article a été créé en 1985, puis modifié en 1997, en 1999, en 2002 et en 2015. Pour chaque version de l’article, il est inscrit juste en dessous du numéro de l’article (au centre de la page) les références du texte qui modifient ou créent l’article. Regarder ces modifications de la loi permet de repérer toute modification dans les règles de calcul du dispositif. Par exemple l’article L521-1 du CSS explique les principes généraux des allocations familiales (nombre minimal d’enfant, variation du montant en fonction du nombre d’enfants, et en fonction des ressources). On voit que la modulation en fonction des ressources est présente dans la version de 2015, mais pas avant, ce qui implique la présence d’une réforme. 

Mais cet article ne précise pas les paramètres utilisés pour l’application de ces règles générales. En bas de la page web de l’article sont listés tout un ensemble d’autres articles qui citent cet article. Parmi eux, des textes règlementaires (commençant par D ou R), dont par exemple l’article D521-1 (qui se trouve [ici](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=20A5000F1D8695359D61A1EDA74258B8.tplgfr21s_3?idArticle=LEGIARTI000030680318&cidTexte=LEGITEXT000006073189&categorieLien=id&dateTexte=)), qui liste notamment les montants des allocations familiales pour chaque enfant à charge. De même que pour l’article L521-1, on a à gauche de la page une liste des différentes versions du texte.

**Attention** : souvent, lorsque les paramètres changent, ils sont déplacés de texte. Par exemple, un paramètre de 2016 peut être présent dans un arrêté de 2016, et celui de 2015 (si différent) dans un arrêté de 2015, qui est distinct. Il est donc important de regarder si d’autres textes n’ont pas été créés.

Pour gagner du temps sur ce point, un bon point de départ est de partir du site [www.service-public.fr](https://www.service-public.fr/) , qui décrit de manière simple les règles d’un nombre important de dispositifs (vrai surtout pour les prestations sociales, et les impôts dans une moindre mesure). Ce site est actualisé de manière continue, et contient pour chaque dispositif les textes officiels associés. Si un autre texte a été créé, il est donc beaucoup plus facile de le repérer par ce biais.

Si ce site ne contient pas ou peu d’informations sur le dispositif étudié, mieux vaut dans tous les cas commencer par une recherche web pour trouver un document décrivant de manière simplifiée le dispositif dans sa version la plus récente, avant de se lancer dans la lecture exhaustive de [www.legifrance.gouv.fr](https://www.legifrance.gouv.fr/) . Puis dans un second temps regarder les textes de loi.

Pour revenir à la partie législative d’un dispositif, la loi est organisée par section, qui sont découpées par dispositif. Toujours dans le cas des allocations familiales, si l’on part l’article L521-1 ([ici](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=19EA4D92D6B2F82DA7DF807F71CF39C1.tplgfr21s_3?idArticle=LEGIARTI000029963006&cidTexte=LEGITEXT000006073189&categorieLien=id&dateTexte=)), on voit en haut de la page web une arborescence, qui indique la présence d’un chapitre dédié à ces allocations. Au faisant défiler les articles, on voit que les allocations familiales sont régies par les articles L521-1 à L521-3. Donc, si l’on veut, en plus d’actualiser des paramètres déjà existants, regarder si d’autres règles ont été créées, il faut faire défiler ces différents articles.


Liste des sources d'informations sur la législation : 

- [www.service-public.fr](https://www.service-public.fr/)
- [www.bofip.impots.gouv.fr](http://bofip.impots.gouv.fr/bofip/1-PGP.html) pour les impôts
- [www.legislation.cnav.fr](https://www.legislation.cnav.fr/Pages/baremes.aspx) pour les prestations CNAV
- [www.legifrance.gouv.fr](https://www.legifrance.gouv.fr/)
- [Liste des JO publiés de janvier 1992 à décembre 1997](http://admi.net/jo/sommaires/), avec le sommaire des décrets et des lois qu’ils contiennent :
- [Lois de Finances promulguées depuis 1958](http://www.legifrance.gouv.fr/affichSarde.do?reprise=true&page=1&idSarde=SARDOBJT000007104806&ordre=null&nature=null&g=ls)
 